$(document).ready(function() {
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '205230039930460',
            xfbml      : true,
            version    : 'v2.8'
        });
        FB.AppEvents.logPageView();
        FB.api(
            '/232563766917957?fields=access_token&access_token=EAAC6p9egllwBAIWOdZC1wyUMBQvyZCVhjZAtpSiNgAZAdpeuQOXGKy0Slq6N4CZBvikrOgZCMY5bXZCDxyuOSAZCZAwC0c7es1QPcWbnv9ZAgg9LKJLldnE94Nw62FuRxc2AyvVhCi2SCdhA6JgRD6ZCSn6IPuIsNAmZCaJu2ZCslZCbUY8QZDZD',
            function (response) {
                console.log(response);
                console.log(response.access_token);
                //FB.api(
                //    '/232563766917957/ratings?access_token='+response.access_token,
                //    function (response) {
                //        if (response.error) {
                //            console.log(response.error.message);
                //            return;
                //        }
                //        response.data.each(function(item) {
                //            if (item.review_text) {
                //                console.log(item);
                //            }
                //        });
                //    }
                //)

            }
        );
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(".video-thumb").click(function() {

        $(".vimeo-container").html("<iframe src=\"http://player.vimeo.com/video/"+$(this).attr('data-video-id')+"\"width=\"100%\" height=\"535\" frameborder=\"0\" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>");
        $("html, body").animate({
            scrollTop: 15
        }, 1000);

        $('.video-thumb').each(function() {
            $(this).removeClass('active');
        });

        $(this).addClass('active');
        videoSize();
    });

    function videoSize() {
        var ratio = 0.453125;
        var iframeWidth = $(".vimeo-container iframe").width();
        $(".vimeo-container iframe").height(iframeWidth*ratio);
    }
    videoSize();

    $(window).resize(function() {
        videoSize();
    });
});
