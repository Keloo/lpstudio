<html>
<head>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<style>
	#cont img {
		opacity: 0.4;
	}

	a:link {color: #9d9c9d;}
	a:visited {color: #9d9c9d;}
	a:active {color: #9d9c9d;}
	a:hover {color: #9d9c9d;}
	a {text-decoration: none;}


	</style>
</head>
<body>

	<div class="container">
		<div class="row">
				<div class="span3">
                    <a href="https://www.facebook.com/LPstudio-Videograf-Nunti-Videograf-Evenimente-232563766917957/">
                        <img style="height: 40px;margin-top:100px; padding-left: 20px;" src="img/fb.png" alt="Facebook">
                    </a>
                </div>
				<div class="span6">
					<center><a href="http://lpstudio.md"><img src="up1.jpg"/></a></center>
				</div>
				<div class="span3" >
					<div style="padding-right: 20px; font-size:20px; color:#9d9c9d; font-family:Century Gothic; margin-top:120px; text-align:right;">

						068 09 22 61
					</div>
				</div>

<?php

$file=explode("\n",file_get_contents("admin/db.txt"));

	echo '	</div>
		<div class="row">

			<div class="span12">
				<div id="video" style="padding-left: 20px; padding-right: 20px;">
					<iframe src="http://player.vimeo.com/video/'.$file[0].'" width="100%" height="510" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
				</div>
				<br />
				<div id="thumbs" class="row" style="">
					<div id="cont" style="padding-left: 20px; padding-right: 20px;">
					';


class HTTPRequest
{
    var $_fp;        // HTTP socket
    var $_url;        // full URL
    var $_host;        // HTTP host
    var $_protocol;    // protocol (HTTP/HTTPS)
    var $_uri;        // request URI
    var $_port;        // port

    // scan url
    function _scan_url()
    {
        $req = $this->_url;

        $pos = strpos($req, '://');
        $this->_protocol = strtolower(substr($req, 0, $pos));

        $req = substr($req, $pos+3);
        $pos = strpos($req, '/');
        if($pos === false)
            $pos = strlen($req);
        $host = substr($req, 0, $pos);

        if(strpos($host, ':') !== false)
        {
            list($this->_host, $this->_port) = explode(':', $host);
        }
        else
        {
            $this->_host = $host;
            $this->_port = ($this->_protocol == 'https') ? 443 : 80;
        }

        $this->_uri = substr($req, $pos);
        if($this->_uri == '')
            $this->_uri = '/';
    }

    // constructor
    function HTTPRequest($url)
    {
        $this->_url = $url;
        $this->_scan_url();
    }

    // download URL to string
    function DownloadToString()
    {
        $crlf = "\r\n";

        // generate request
        $req = 'GET ' . $this->_uri . ' HTTP/1.0' . $crlf
            .    'Host: ' . $this->_host . $crlf
            .    $crlf;

        // fetch
        $this->_fp = fsockopen(($this->_protocol == 'https' ? 'ssl://' : '') . $this->_host, $this->_port);
        fwrite($this->_fp, $req);
        while(is_resource($this->_fp) && $this->_fp && !feof($this->_fp))
            $response .= fread($this->_fp, 1024);
        fclose($this->_fp);

        // split header and body
        $pos = strpos($response, $crlf . $crlf);
        if($pos === false)
            return($response);
        $header = substr($response, 0, $pos);
        $body = substr($response, $pos + 2 * strlen($crlf));

        // parse headers
        $headers = array();
        $lines = explode($crlf, $header);
        foreach($lines as $line)
            if(($pos = strpos($line, ':')) !== false)
                $headers[strtolower(trim(substr($line, 0, $pos)))] = trim(substr($line, $pos+1));

        // redirection?
        if(isset($headers['location']))
        {
            $http = new HTTPRequest($headers['location']);
            return($http->DownloadToString($http));
        }
        else
        {
            return($body);
        }
    }
}


for ($i=0; $i<count($file); $i++)
{
	$r = new HTTPRequest('http://vimeo.com/api/v2/video/'.$file[$i].'.xml');
	$img=$r->DownloadToString();
	$i1=strpos($img,"<thumbnail_medium>").' ';
	$i2=strpos($img,"</thumbnail_medium>");
	$img=substr($img,$i1+18,$i2-$i1-18);

	echo '
		<img src="'.$img.'" class="span2" onclick="f(\''.$file[$i].'\')"/>
	';
}

?>

					</div>
				</div>
			</div>

		</div>
		<div class="row">

			<div class="span12">
				<center>
					<div style="font-size:20px; color:#9d9c9d; line-height: 150%; font-family:Century Gothic; margin-top:5px;">

						<span style="font-size:18px">Copyright &copy; 2013, LPstudio </span>
						<hr align="center" width="350" size="4" color="#cccccc" style="margin-top:-1px; margin-bottom:0px" />
						<a href="http://byandrew.md" target="_blank"> Fotograf </a> <br />
						<a href="1/"> Muzica de petrecere </a>

					</div><br />
				</center>
			</div>

		</div>
	</div>


	<!-- The Javascript-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js" ></script>

<script>

function f(str){
	document.getElementById("video").innerHTML = "<iframe src=\"http://player.vimeo.com/video/"+str+"\"width=\"100%\" height=\"535\" frameborder=\"0\" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
}

$("#cont img").mouseover(function(){
	$(this).stop().animate({
        'opacity':'1.0'
    },200);
})
$("#cont img").mouseout(function(){
	$(this).stop().animate({
        'opacity':'0.4'
    },200);
})

function makeScrollable($wrapper, $container, contPadding){
    //Get menu width
    var divWidth = $wrapper.width();

    //Remove scrollbars
    $wrapper.css({
        overflow: 'hidden'
    });

    //Find last image container
    var lastLi = $container.find('img:last-child');
    $wrapper.scrollLeft(0);
    //When user move mouse over menu
    $wrapper.unbind('mousemove').bind('mousemove',function(e){

        //As images are loaded ul width increases,
        //so we recalculate it each time
        var ulWidth = lastLi[0].offsetLeft + lastLi.outerWidth() + contPadding;

        var left = (e.pageX - $wrapper.offset().left) * (ulWidth-divWidth) / divWidth;
        $wrapper.scrollLeft(left);
    });
}

makeScrollable($('#thumbs'),$('#cont'),-150);

</script>

</body>
</html>
