<html>
<head>
    <title>LP Studio</title>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="public/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/css/custom.css">
</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8&appId=1768121783474744";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<div class="container">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <div class="row">
                <div class="col-sm-3 fb-col hidden-xs">
                    <a target="_blank" href="https://www.facebook.com/LPstudio-Videograf-Nunti-Videograf-Evenimente-232563766917957/"><img class="fb-image" src="public/images/fb.png" alt="Facebook"></a>
                    <a target="_blank" href="https://vimeo.com/user9567756"><img class="vimeo-image" src="public/images/vimeo.png" alt="Vimeo"></a>
                </div>
                <div class="col-sm-6 col-xs-12 text-center">
                    <img class="img-responsive" src="public/images/logo.jpg" alt="Logo">
                </div>
                <div class="col-sm-3 phone-number-col hidden-xs">
                    <span class="phone-number pull-right">068 09 22 61</span>
                </div>
                <div class="col-xs-12 visible-xs">
                    <a target="_blank" href="https://www.facebook.com/LPstudio-Videograf-Nunti-Videograf-Evenimente-232563766917957/"><img class="fb-image1" src="public/images/fb.png" alt="Facebook"></a>
                    <a target="_blank" href="https://vimeo.com/user9567756"><img class="vimeo-image1" src="public/images/vimeo.png" alt="Vimeo"></a>
                    <span class="phone-number1 pull-right">068 09 22 61</span>
                </div>
            </div>

            <div class="row">
                <div class="vimeo-container">
                    <iframe src="http://player.vimeo.com/video/<?=$videosInfo[0]->id;?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                </div>
            </div>

            <br>
            <div class="row">
                <?php
                    foreach ($videosInfo as $video) {
                        $id = $video->id;
                        $thumbnail = str_replace('200', '300',$video->thumbnail_medium);
                        echo "<div class='col-xs-6 col-md-3 video-col'><img class='img-responsive video-thumb' data-video-id='{$id}' src='{$thumbnail}' alt='Thumbnail'></div>";
                    }
                ?>
            </div>
            <br>

            <div class="row">
                <div class="col-xs-12 footer-col text-center">
                    <span class="copyright">Copyright &copy; <?=date("Y");?>, LPstudio Wedding Videography </span>
                    <hr align="center" width="350" size="5" color="#cccccc" />
                    <a target="_blank" href="https://www.facebook.com/www.lpstudio.md"> Fratii parfeni </a>
                </div>
            </div>
            <br>
            <br>

            <div class="row reviews">
                <h1 class="text-center">Review-uri</h1>
                <div class="col-xs-12">
                    <?php
                    foreach ($reviews as $review) {
                        if ($review->review_text) {
                            echo "<blockquote><p>{$review->review_text}</p> <br> <span>- {$review->reviewer->name}</span></blockquote>";
                        }
                    }
                    ?>
                </div>
            </div>
            <div id="disqus_thread"></div>
            <script>
                 var disqus_config = function () {
                     this.page.url = "http://lpstudio.md";
                     this.page.identifier = 'lpstudio';
                 };
                (function() {
                    var d = document, s = d.createElement('script');
                    s.src = '//lpstud.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

            <div class="col-xs-12 fake-block"></div>
        </div>
    </div>
</div>

<!-- Javascripts -->
<script src="public/js/jquery.min.js"></script>
<script src="public/js/bootstrap.min.js"></script>
<script src="public/js/custom.js"></script>

</body>
</html>
